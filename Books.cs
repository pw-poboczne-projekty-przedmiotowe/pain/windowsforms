﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WinFormsApp
{
    public class Books
    {
        public List<Book> BookList = new List<Book>();

        public event Action<Book> AddBookEvent;
        public event Action<Book> EditBookEvent;
        public event Action<Book> DeleteBookEvent;

        public void AddBook(Book book)
        {
            BookList.Add(book);
            AddBookEvent?.Invoke(book);
        }

        public void DeleteBook(Book Book)
        {
            BookList.Remove(Book);
            DeleteBookEvent?.Invoke(Book);
        }

        public void EditBook(Book Book)
        {
            EditBookEvent?.Invoke(Book);
        }
    }
}
