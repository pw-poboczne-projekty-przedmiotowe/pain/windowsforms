﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WinFormsApp
{
    public class Book
    {
        public String Title
        {
            get;
            set;
        }

        public String Author
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public int genre_id;
        static public String[] genresList = { "novel", "criminal", "sci-fi" };

        public String getGenreStr()
        {
            if(genre_id >= 0 && genre_id <genresList.Length)
            {
                return genresList[genre_id];
            }else
            {
                return "err";
            }
        }

        public void setGenre(String genre_str)
        {
            for(int i =0;i<genresList.Length;i++)
            {
                if(genresList[i]==genre_str)
                {
                    this.genre_id = i;
                    return;
                }
            }
            this.genre_id = -1;
        }
    }
}
