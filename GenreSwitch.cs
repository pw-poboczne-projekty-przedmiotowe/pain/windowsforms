﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Design;
using System.Windows.Forms.Design;

namespace WinFormsApp
{

    public partial class GenreSwitch : UserControl
    {
        private int genre_id = 0;
        public event Action<int> changeGenreEvent;
        public GenreSwitch()
        { }

        public GenreSwitch(int id)
        {
            this.genre_id = id;
        }

        [BrowsableAttribute(true)]
        [EditorAttribute(typeof(GenreSwitchEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public int GenreID
        {
            get
            { return genre_id; }
            set
            { genre_id = value; Invalidate(); }
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            changeGenre((this.genre_id + 1) % 3);
            this.Refresh();
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            var size = new Size(this.Width, this.Height);
            if(genre_id == 0)
                e.Graphics.DrawImage(global::WinFormsApp.Properties.Resources.novel, new Rectangle(new Point(0, 0), size));
            else if (genre_id == 1)
                e.Graphics.DrawImage(global::WinFormsApp.Properties.Resources.crime, new Rectangle(new Point(0, 0), size));
            else if (genre_id == 2)
                e.Graphics.DrawImage(global::WinFormsApp.Properties.Resources.scifi, new Rectangle(new Point(0, 0), size));

        }

        public void changeGenre(int genre_id)
        {
            this.genre_id = genre_id;
            changeGenreEvent?.Invoke(genre_id);
        }
        public class GenreSwitchEditor : System.Drawing.Design.UITypeEditor
        {
            public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, System.IServiceProvider ISprovider, object value)
            {
                IWindowsFormsEditorService windowsFormsEditorService = (IWindowsFormsEditorService)ISprovider.GetService(typeof(IWindowsFormsEditorService));
                if (windowsFormsEditorService != null)
                {
                    GenreSwitch GenreSwitch = new GenreSwitch((int)value);
                    windowsFormsEditorService.DropDownControl(GenreSwitch);
                    return GenreSwitch.GenreID;
                }
                return value;
            }
            public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
            {
                return UITypeEditorEditStyle.DropDown;
            }

            // Draws a representation of the property's value.
            public override void PaintValue(System.Drawing.Design.PaintValueEventArgs e)
            {
                var size = new Size(e.Bounds.Width, e.Bounds.Height);
                int genre_id = (int)e.Value;
                if (genre_id == 0)
                    e.Graphics.DrawImage(global::WinFormsApp.Properties.Resources.novel, new Rectangle(new Point(0, 0), size));
                else if (genre_id == 1)
                    e.Graphics.DrawImage(global::WinFormsApp.Properties.Resources.crime, new Rectangle(new Point(0, 0), size));
                else if (genre_id == 2)
                    e.Graphics.DrawImage(global::WinFormsApp.Properties.Resources.scifi, new Rectangle(new Point(0, 0), size));
            }

            // Indicates whether the UITypeEditor supports painting a 
            // representation of a property's value.
            public override bool GetPaintValueSupported(System.ComponentModel.ITypeDescriptorContext context)
            {
                return true;
            }
        }
    }
}
