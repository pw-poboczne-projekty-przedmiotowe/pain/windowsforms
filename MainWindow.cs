﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp
{
    public partial class MainWindow : Form
    {
        public int opened_windows = 0;
        public Books books = new Books();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void add_window_Click(object sender, EventArgs e)
        {
            BookForm newform = new BookForm(this);
            newform.MdiParent = this;
            newform.Show();
            opened_windows++;
        }

        private void button_edit_Click(object sender, EventArgs e)
        {

        }

        private void button_delete_Click(object sender, EventArgs e)
        {

        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }

        public void update_number_of_elements(Int64 number)
        {
            this.toolStripStatusLabelElementNumber.Text = "Number of elements: " + number.ToString();
        }
    }
}
