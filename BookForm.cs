﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp
{
    public partial class BookForm : Form
    {
        private MainWindow parent;
        private int sorting_type = 0; // 0-none 1- before 2000 2 - after 2000
        private int number_of_items;
        public int NumberOfItems
        {
            get { return number_of_items; }
            set
            {
                if (value == 0)
                {
                    change_active_buttons(false);
                }
                else
                {
                    change_active_buttons(true);
                }
                number_of_items = value;
                parent.update_number_of_elements(NumberOfItems);
            }
        }
        public BookForm(MainWindow parent)
        {
            InitializeComponent();
            this.parent = parent;
            reload_list();
            toolStripComboBoxSorting.SelectedIndex = 0;
        }

        private void form_closing(object sender, FormClosingEventArgs e)
        {
            if (this.parent.opened_windows == 1)
            {
                e.Cancel = true;
            }
            else
            {
                this.parent.opened_windows--;
                e.Cancel = false;
            }

        }

        private void BookForm_Load(object sender, EventArgs e)
        {
            parent.books.AddBookEvent += book_added;
            parent.books.DeleteBookEvent += book_removed;
            parent.books.EditBookEvent += book_changed;
        }

        private void book_added(Book b)
        {
            if (this.belongs_to_list_after_filtering(b))
            {
                ListViewItem item = new ListViewItem();
                item.Tag = b;
                load_item_data(item);
                listView1.Items.Add(item);
                NumberOfItems++;
            }
        }

        private void book_removed(Book b)
        {
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                if (b.Equals(listView1.Items[i].Tag))
                {
                    listView1.Items[i].Remove();
                    NumberOfItems--;
                }
            }
        }

        private void book_changed(Book b)
        {
            bool belongs_after_filtering = this.belongs_to_list_after_filtering(b);
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                if (b.Equals(listView1.Items[i].Tag))
                {
                    if (belongs_after_filtering)
                        load_item_data(listView1.Items[i]);
                    else
                        book_removed(b);
                    return;

                }
            }

            if (belongs_after_filtering)
                book_added(b);
        }

        private void book_add_click(object sender, EventArgs e)
        {
            BookEditorForm newBookForm = new BookEditorForm(null, parent.books);
            if (newBookForm.ShowDialog() == DialogResult.OK)
            {
                Book new_book = new Book();
                new_book.Title = newBookForm.title;
                new_book.Author = newBookForm.author;
                new_book.Date = newBookForm.publishDate;
                new_book.genre_id = newBookForm.genre_number;
                parent.books.AddBook(new_book);
            }
        }

        private void book_remove_click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count >= 1)
            {
                Book b = (Book)listView1.SelectedItems[0].Tag;
                parent.books.DeleteBook(b);
            }
        }

        private void book_change_click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                Book b = (Book)listView1.SelectedItems[0].Tag;
                BookEditorForm newBookForm = new BookEditorForm(b, parent.books);
                if (newBookForm.ShowDialog() == DialogResult.OK)
                {
                    b.Title = newBookForm.title;
                    b.Author = newBookForm.author;
                    b.Date = newBookForm.publishDate;
                    b.genre_id = newBookForm.genre_number;
                    parent.books.EditBook(b);
                }
            }
        }

        private void BookForm_Activated(object sender, EventArgs e)
        {
            ToolStripManager.Merge(toolStrip1, parent.toolStrip1);
            parent.update_number_of_elements(NumberOfItems);
        }

        private void BookForm_Deactivate(object sender, EventArgs e)
        {
            ToolStripManager.RevertMerge(parent.toolStrip1, toolStrip1);
        }

        private void load_item_data(ListViewItem item)
        {
            Book book = (Book)item.Tag;
            while (item.SubItems.Count < 4)
                item.SubItems.Add(new ListViewItem.ListViewSubItem());
            item.SubItems[0].Text = book.Title;
            item.SubItems[1].Text = book.Author;
            item.SubItems[2].Text = book.Date.ToShortDateString();
            item.SubItems[3].Text = book.getGenreStr();
        }

        private void reload_list()
        {
            listView1.Items.Clear();
            NumberOfItems = 0;
            for (int i = 0; i < parent.books.BookList.Count; i++)
            {
                Book b = parent.books.BookList[i];
                if (belongs_to_list_after_filtering(b))
                {
                    ListViewItem item = new ListViewItem();
                    item.Tag = b;
                    load_item_data(item);
                    listView1.Items.Add(item);
                    number_of_items++;
                }
            }
            NumberOfItems = number_of_items;
        }

        private void sorting_changed(object sender, EventArgs e)
        {
            if (toolStripComboBoxSorting.SelectedIndex != sorting_type)
            {
                sorting_type = toolStripComboBoxSorting.SelectedIndex;
                reload_list();
            }
        }

        private bool belongs_to_list_after_filtering(Book book)
        {
            if (sorting_type == 0)
                return true;

            bool is_after_2000 = book.Date.Year > 2000;

            if (is_after_2000 && sorting_type == 2)
                return true;

            if (!is_after_2000 && sorting_type == 1)
                return true;

            return false;
        }

        private void change_active_buttons(bool is_active)
        {
            this.toolStripButtonEdit.Enabled = is_active;
            this.toolStripButtonDelete.Enabled = is_active;
            this.toolStripMenuEdit.Enabled = is_active;
            this.toolStripMenuDelete.Enabled = is_active;
        }
    }
}
