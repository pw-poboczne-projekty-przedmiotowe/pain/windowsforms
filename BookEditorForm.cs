﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp
{
    public partial class BookEditorForm : Form
    {
        private Books book_list;
        private Book book;
        public int genre_number=0;
        public BookEditorForm(Book book, Books list_books )
        {
            InitializeComponent();
            this.book_list = list_books;
            this.book = book;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            if(book != null)
            {
                textBoxTitle.Text = book.Title;
                textBoxAuthor.Text = book.Author;
                dateTimePicker1.Value = book.Date;
                genre_number = book.genre_id;
            }
            update_genre_label(genre_number);
            genreControl1.changeGenreEvent += update_genre_label;
        }

        public string title
        {
            get { return textBoxTitle.Text; }
        }
        public string author
        {
            get { return textBoxAuthor.Text; }
        }
        public DateTime publishDate
        {
            get { return dateTimePicker1.Value; }
        }

        private void textBoxTitle_Validating(object sender, CancelEventArgs e)
        {
            if (Equals(textBoxTitle.Text,""))
            {
                e.Cancel = true;
                errorProvider.SetError(textBoxTitle,"Cannot be empty");
            }else
            {
                errorProvider.Clear();
            }
        }

        private void textBoxAuthor_Validating(object sender, CancelEventArgs e)
        {
            if (Equals(textBoxAuthor.Text, ""))
            {
                e.Cancel = true;
                errorProvider.SetError(textBoxAuthor, "Cannot be empty");
            }else
            {
                errorProvider.Clear();
            }
        }

        private void update_genre_label(int id)
        {
            this.genre_number = id;
            String[] genresList = { "novel", "criminal", "sci-fi" };
            labelGenre.Text = genresList[id];
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
